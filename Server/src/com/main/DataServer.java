package com.main;

import networking.udp.UDPClient;

/**
cmd -> ipconfig/all -> IPv4 (Preferred)
 **/

public class DataServer {
    public static void runUDPClient(String[] args) {
        new Thread(() -> UDPClient.main(args)).start();
    }
    public static void main(String[] args) {
        runUDPClient(args);
    }
}
