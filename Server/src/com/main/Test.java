package com.main;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {
        List<Integer> list = Arrays.stream(new int[] {1,2,3,4,5}).boxed().collect(Collectors.toList());

        int n = 6;
        int c = list.stream()
                .min(Comparator.comparingInt(i -> Math.abs(i - n)))
                .orElseThrow(() -> new NoSuchElementException("[!] Unfortunately we could not find any backup"));
        System.out.println();
    }
}
