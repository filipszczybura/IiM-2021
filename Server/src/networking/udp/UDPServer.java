package networking.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPServer {

    public static boolean isConnected = false;

    public static void main(String[] args) throws IOException {
        int serverPort = 9876;
        DatagramSocket socket = new DatagramSocket(serverPort);
        byte[] buffer = new byte[1024];
        isConnected = true;
        while (isConnected) {
            try {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                socket.receive(request);
                DatagramPacket reply = new DatagramPacket(
                        request.getData(),
                        request.getLength(),
                        request.getAddress(),
                        request.getPort()
                        );
                socket.send(reply);
            } catch (IOException ex) {
                isConnected = false;
                System.out.println("SERVER'S DOWN - exception");
                Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
