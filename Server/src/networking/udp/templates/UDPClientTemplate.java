package networking.udp.templates;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPClientTemplate {

    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.out.println("Usage: UDPCLient <msg> <server host name>");
                System.exit(-1);
            }
            DatagramSocket socket = new DatagramSocket();
            byte[] msg = args[0].getBytes();
            System.out.println("Sent: " + args[0]);

            InetAddress host = InetAddress.getByName(args[1]);
            int serverPort = 9876;
            DatagramPacket request = new DatagramPacket(
                    msg,
                    args[0].length(),
                    host,
                    serverPort
            );
            socket.send(request);

            byte[] buffer = new byte[1024];
            DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
            socket.receive(reply);

            System.out.println("Reply: " + Arrays.toString(reply.getData()));
            socket.close();

        } catch (IOException ex) {
            Logger.getLogger(UDPClientTemplate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
