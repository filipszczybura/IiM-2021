package networking.udp;

import data.types.Packet;
import tools.Tools;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 @author szczy
 @apiNote Remember to run the UDPServer in parallel before running the UDPClient!
 **/

public class UDPClient {

    private static final String DATABASE = "Database/database.txt";
    private static final String COMMUNICATIONS = "Communication";
    private static final String BACKUP = "Backup";

    public static int generateRandomInteger(int min, int max) {
        return new Random().nextInt((max + 1) - min) + min;
    }

    public static void runUDPServer(String[] args) {
        new Thread(() -> {
            try {
                UDPServer.main(args);
            } catch (IOException ioException) {
                UDPServer.isConnected = false;
                System.out.println("UDPServer Thread Exception");
                ioException.printStackTrace();
            }
        }).start();
    }

    public static List<String> getFileContent(String fileName) {
        List<String> content = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            content = reader.lines().collect(Collectors.toList());
        } catch (FileNotFoundException ex) {
            System.out.println("File does not exist:\t" + ex);
        }
        return content;
    }

    public static boolean isResponseEqualRequest(String requestFile, String responseFile) {
        List<String> requestContent = getFileContent(requestFile);
        List<String> responseContent = getFileContent(responseFile);
        if (requestContent.size() != responseContent.size()) {
            return false;
        }
        for (int i = 2; i < requestContent.size(); i++) {
            if (!requestContent.get(i).equals(responseContent.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static List<Packet> getDatabase(String queryFile) {
        List<Packet> dataTypes = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(queryFile));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                List<String> parameters = new ArrayList<>(Arrays.asList(line.split(",", 5)));
                if (parameters.size() == 5) {
                    Class<?> objectClass = Class.forName("data.types." + parameters.get(0));
                    String device = parameters.get(1);
                    long date = Long.parseLong(parameters.get(2));
                    int channelNr = Integer.parseInt(parameters.get(3));
                    String description = parameters.get(4);
                    if (parameters.get(0).equals("Spectrum") | parameters.get(0).equals("TimeHistory")) {
                        List<Integer> buffer = Arrays.stream(new Integer[32])
                                .map(el -> generateRandomInteger(0, 256))
                                .collect(Collectors.toList());
                        Constructor<?> ctor = objectClass.getConstructor(String.class, long.class, int.class, String.class, List.class);
                        dataTypes.add((Packet) ctor.newInstance(device, date, channelNr, description, buffer));
                    } else {
                        Constructor<?> ctor = objectClass.getConstructor(String.class, long.class, int.class, String.class);
                        dataTypes.add((Packet) ctor.newInstance(device, date, channelNr, description));
                    }
                }
            }
        } catch (
                FileNotFoundException | ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            System.out.println(ex);
            return dataTypes;
        }
        return dataTypes;
    }

    public static String getPathToDatabase() {
        String input = new Scanner(System.in).nextLine();
        return input.isEmpty() ? DATABASE : input;
    }

    public static HashMap<String, Collection<?>> getInputParameters() {
        Scanner scanner = new Scanner(System.in);
        HashMap<String, Collection<?>> mapping = new HashMap<>();
        System.out.print(" - Device: ");
        String line = scanner.nextLine();
        if(line.matches("^(([\\w\\-]+\\s?)+(,\\s?)?)+$")) {
            List<String> deviceQueries = Arrays.asList(line.split(",\\s?"));
            mapping.put("device", deviceQueries);
        }

        System.out.print(" - ChannelNr: ");
        line = scanner.nextLine();
        if (line.matches("^((\\d{1,2})(,\\s?)?)+$")) {
            List<Integer> channelNrQueries = Stream.of(line.split(",\\s?"))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            mapping.put("channelNr", channelNrQueries);
        }

        System.out.print(" - Date (unix timestamp): ");
        line = scanner.nextLine();
        if (line.matches("^((\\d{10})(,\\s?)?)+$")) {
            List<Long> dateQueries = Stream.of(line.split(",\\s?"))
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            mapping.put("date", dateQueries);
        }

        System.out.print(" - Description: ");
        line = scanner.nextLine();
        if (!line.isEmpty()) {
            List<String> descriptionQueries = Arrays.asList(line.split(",\\s?"));
            mapping.put("description", descriptionQueries);
        }
        return mapping;
    }

    public static HashMap<String, Collection<?>> getSearchQueries() {
        System.out.print("[?] Would you like to search for any specific data in the database? y/[n]: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        if (line.equals("y")) {
            System.out.println("[>>] Please provide search keywords:");
            return getInputParameters();
        } else {
            System.out.println("[!] No keywords were provided. Looping over the complete database ...");
        }
        return new HashMap<>();
    }

    public static Set<String> listDirectory(String dir) {
        try (Stream<Path> stream = Files.list(Path.of(dir))) {
            return stream
                    .filter(Files::isDirectory)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        } catch (IOException ex) {
            return new HashSet<>();
        }
    }

    public static String getTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return dateFormat.format(timestamp);
    }

    public static void backup(Set<Packet> suitable) {
        System.out.print("\n[??] Would you like to create a backup? y/[n]: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        if (line.equals("y")) {
            String timestamp = getTimestamp();
            String fileName = "suitable_%s.txt".formatted(timestamp);
            String outputPath = Paths.get(BACKUP, timestamp, fileName).toString();
            for (Packet dataType : suitable) {
                String entry = dataType.getEntry();
                saveToFile(entry, outputPath);
            }
            System.out.printf("[!] Backup successfully exported as %s%n", outputPath);
        } else {
            System.out.println("[!] No backup created!");
        }
    }

    public static String formatDate(Long timestamp) {
        DateFormat originalFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        DateFormat targetFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            Date date = originalFormat.parse(String.valueOf(timestamp));
            return targetFormat.format(date);
        } catch (ParseException ex) {
            return timestamp.toString();
        }
    }

    public static Set<Packet> getLatestBackup() {
        Long timestamp = Long.parseLong(getTimestamp());
        Set<Long> backupFolders = listDirectory(BACKUP).stream().map(Long::parseLong).collect(Collectors.toSet());
        if (backupFolders.size() > 0) {
            long lastBackup = backupFolders.stream()
                    .min(Comparator.comparingLong(i -> Math.abs(i - timestamp)))
                    .orElseThrow(() -> new NoSuchElementException("[!] Unfortunately we could not find any backup"));
            String backupPath = Paths.get(
                    BACKUP,
                    String.valueOf(lastBackup),
                    "suitable_%s.txt".formatted(lastBackup))
                    .toString();
            System.out.printf("[!] Getting latest backup data from %s\n", formatDate(timestamp));
            return new HashSet<>(getDatabase(backupPath));
        }
        return new HashSet<>();
    }

    public static Set<Packet> getBackup() {
        System.out.print("[??] Would you like to load any backup data? y/[n]: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        if (line.equals("y")) {
            System.out.print("[>>] Provide file path, date or press enter to load the latest backup: ");
            line = scanner.nextLine();
            if (new File(line).exists()) {
                System.out.printf("[!] Getting backup data from %s\n", line);
                return new HashSet<>(getDatabase(line));
            }
            else if (line.matches("^\\d{14}$")) {
                System.out.printf("[!] Getting backup data based on provided date %s\n", line);
                String backupFile = Paths.get(
                        BACKUP,
                        line,
                        "suitable_%s.txt".formatted(line)
                ).toString();
                return new HashSet<>(getDatabase(backupFile));
            } else {
                Set<Packet> suitable = getLatestBackup();
                if (suitable.size() == 0) {
                    System.out.println("[!] Empty backup - nothing to load\n");
                }
                return suitable;
            }
        }
        System.out.println("[!] No data has been loaded from backup\n");
        return new HashSet<>();
    }

    public static void saveToFile(String content, String outputPath) {
        try {
            Path outputFile = Paths.get(outputPath);
            Path parentDir = outputFile.getParent();
            if (Files.notExists(parentDir)) {
                Files.createDirectories(parentDir);
            }
            Files.write(
                    outputFile,
                    Collections.singletonList(content),
                    StandardCharsets.UTF_8,
                    Files.exists(outputFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE
            );
        } catch (IOException ioe) {
            System.out.println("[!] Provided path does not exist!");
        }
    }

    public static void updateDatabase() {
        System.out.println("\n--------------------");
        System.out.print("[?] Would you like to update the database? y/[n]: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        if (line.equals("y")) {
            System.out.print("" +
                    "If you want to\n" +
                    "a) Create a new file with database, please provide the output path\n" +
                    "b) Append to " + DATABASE + ", press enter...\n"
            );

            final String databasePath = getPathToDatabase();
            final String[] DATA_TYPES = {"TimeHistory", "Spectrum", "Alarm"};

            if (databasePath.equals(DATABASE)) {
                System.out.println("[!] New entries will be appended to " + DATABASE);
            } else {
                System.out.println("[!] New entries will be included in the " + databasePath);
            }

            System.out.println("[*] Entry should consist of comma separated parameters for DataType, Device, Date (Unix timestamp), ChannelNr and Description\n");
            while (true) {
                System.out.print("[>>] Provide entry to add: ");
                line = scanner.nextLine();
                if (line.isEmpty()) {
                    System.out.println("[!] No more entries to update.");
                    break;
                }
                List<String> parameters = new ArrayList<>(Arrays.asList(line.split(", ?", 5)));
                if (parameters.size() == 5) {

                    // DataType [String]
                    String dataType = parameters.get(0);
                    if (!Arrays.asList(DATA_TYPES).contains(dataType)) {
                        dataType = DATA_TYPES[generateRandomInteger(0, 3)];
                    }

                    // Device [String]
                    String device = parameters.get(1);

                    // Date [long]
                    long date;
                    if (parameters.get(2).matches("\\d{10}")) {
                        date = Long.parseLong(parameters.get(2));
                    } else {
                        date = Instant.now().getEpochSecond();
                    }

                    // ChannelNr [int]
                    int channelNr;
                    try {
                        channelNr = Integer.parseInt(parameters.get(3));
                    } catch (NumberFormatException ex) {
                        channelNr = 0;
                    }

                    // Description
                    String description = parameters.get(4);

                    String entry = String.join(",", new String[]{
                            dataType, device, String.valueOf(date), String.valueOf(channelNr), description
                    });

                    saveToFile(entry, databasePath);
                    System.out.println("[!] Successfully appended entry: '" + entry + "' to " + databasePath + "\n");

                } else {
                    System.out.println("Input should consist of 5 comma space separated parameters:");
                    System.out.println("DataType,Device,Date,ChannelNr");
                    break;
                }
            }
        } else {
            System.out.println("[!] No database was updated");
        }
    }

    public static Set<Packet> getSuitableMatches(List<Packet> database) {
        Set<Packet> suitable = getBackup();
        if (suitable.size() == 0) {
            HashMap<String, Collection<?>> queries = getSearchQueries();
            if (queries.size() > 0) {
                System.out.print("\n[?] Search for entries in reference to all (y) or any [n] matching queries: ");
                Scanner scanner = new Scanner(System.in);
                String line = scanner.nextLine();
                boolean isIntersection = line.equals("y");
                if (isIntersection) {
                    System.out.println("[!] Data filtered based on intersection between database and queries.");
                } else {
                    System.out.println("[!] Data filtered based on union between database and queries.");
                }
                for (Packet dataType : database) {
                    List<Map.Entry<String, Collection<?>>> filtered = dataType.getFilteredData(queries);
                    if (filtered.size() > 0) {
                        if (isIntersection) {
                            if (filtered.size() == queries.size()) {
                                suitable.add(dataType);
                            }
                        } else {
                            suitable.add(dataType);
                        }
                    }
                }
                return suitable;
            }
            return new HashSet<>(database);
        }
        return suitable;
    }

    public static void main(String[] args) {
        try {
            if (args.length < 1) {
                System.out.println("Usage: UDPCLient <msg> <server host name>");
                System.exit(-1);
            }

            // Run the UDPServer in parallel thread
            runUDPServer(args);

            // Get user input and database data types
            System.out.print("[>>] Please provide path to Database: ");
            String databasePath = getPathToDatabase();
            System.out.println("[!] DataTypes will be aggregated from: " + databasePath + "\n");
            List<Packet> database = getDatabase(databasePath);
            if (database.size() > 0) {
                // Create a new datagram socket
                DatagramSocket socket = new DatagramSocket();

                // Preferred IPv4 address (cmd: ipconfig/all)
                InetAddress host = InetAddress.getByName(args[0]);
                int serverPort = 9876;

                // Get matches
                Set<Packet> suitable = getSuitableMatches(database);

                if (suitable.size() > 0) {
                    Iterator<Packet> dataTypeIterator = suitable.iterator();
                    for (int i = 0; i < suitable.size(); i++) {
                        Packet dataType = dataTypeIterator.next();

                        System.out.println("\n--------------------");
                        System.out.println("[" + (i + 1) + "] Connected to the UDPServer\n");

                        // Save Packet request
                        String requestFile = Paths.get(
                                COMMUNICATIONS,
                                "data_type_request_v" + (i + 1) + ".txt"
                        ).toString();
                        String requestContent = dataType.getCommunication("<Server request>");
                        saveToFile(requestContent, requestFile);

                        // Serialize Packet object to an array of bytes
                        byte[] data = Tools.serialize(dataType);
                        System.out.println("Sent: " + Arrays.toString(data));

                        // Request datagram packet will store data query for the server
                        DatagramPacket request = new DatagramPacket(
                                Objects.requireNonNull(data),
                                data.length,
                                host,
                                serverPort
                        );

                        // Send serialized datagram packet to the server
                        socket.send(request);

                        // The reply datagram packet will store data returned by the server
                        DatagramPacket reply = new DatagramPacket(data, data.length);
                        System.out.println("Reply: " + Arrays.toString(reply.getData()));

                        // Acquire data returned by the server
                        socket.receive(reply);

                        // Store deserialized data in null Packet object.
                        Packet read = (Packet) Tools.deserialize(data);
                        System.out.println("\n[<<] Reading deserialized data...\n\n" + read.toString());
                        String responseFile = Paths.get(
                                COMMUNICATIONS,
                                "data_type_response_v" + suitable.size() + ".txt"
                        ).toString();
                        String responseContent = dataType.getCommunication("<Server response>");
                        saveToFile(responseContent, responseFile);

                        // Verify whether response corresponds to the request
                        if (isResponseEqualRequest(requestFile, responseFile)) {
                            System.out.println("[!] Communication successful. Server response corresponds to the request.");
                        } else {
                            System.out.println("[!] Communication unsuccessful. Server response does not correspond to the request.");
                        }
                    }
                } else {
                    System.out.println("[!] No suitable matches found.\n");
                }

                // Update Queries
                updateDatabase();

                // Make backup of suitable entries
                backup(suitable);

                // Close the datagram socket
                socket.close();
            } else {
                System.out.println("[!] Database is empty!");
            }

        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex);
            Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            UDPServer.isConnected = false;
            System.out.println("\n--------------------");
            System.out.println("[**] Thank you for taking advantage of DataServer! :)\n");
            System.exit(0);
        }
    }
}
