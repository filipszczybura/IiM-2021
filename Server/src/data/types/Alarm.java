package data.types;

public class Alarm extends Packet {

    private double threshold;
    private int direction;

    public Alarm() {
        super();
        this.threshold = 0;
        this.direction = 0;
    }

    public Alarm(
            String device,
            String description,
            long date,
            int channelNr,
            double threshold,
            int direction
    ) {
        super(device, date, channelNr, description);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.setDirection(direction);
    }

    public Alarm(String device, long date, int channelNr, String description) {
        super(device, date, channelNr, description);
        this.threshold = 0.5;
        this.direction = 1;
    }

    public double getThreshold() { return this.threshold; }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public int getDirection() {
        return this.direction;
    }

    public void setDirection(int direction) {
        if (direction >= -1 && direction <= 1) {
            this.direction = direction;
        } else {
            this.direction = 0;
        }
    }
}
