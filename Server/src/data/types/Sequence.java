package data.types;

import java.util.ArrayList;
import java.util.List;

public abstract class Sequence<T> extends Packet {

    protected String unit;
    protected double resolution;
    protected List<T> buffer;

    public Sequence() {
        super();
        this.unit = "Default unit";
        this.resolution = 0.1;
        this.buffer = new ArrayList<>();
    }

    public Sequence(
            String device,
            String description,
            long date,
            int channelNr,
            String unit,
            double resolution,
            List<T> buffer
    ) {
        super(device, date, channelNr, description);
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    public Sequence(String device, long date, int channelNr, String description, List<T> buffer) {
        super(device, date, channelNr, description);
        this.unit = "Queried unit";
        this.resolution = 0.05;
        this.buffer = buffer;
    }

}
