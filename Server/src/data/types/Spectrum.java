package data.types;

import java.util.List;

public class Spectrum<T> extends Sequence<T> {

    private String scaling;

    public Spectrum() {
        super();
        this.scaling = "linalg";
    }

    public Spectrum(
            String device,
            String description,
            long date,
            int channelNr,
            String unit,
            double resolution,
            List<T> buffer,
            String scaling
    ) {
        super(
                device,
                description,
                date,
                channelNr,
                unit,
                resolution,
                buffer
        );
        this.setScaling(scaling);
    }

    public Spectrum(String device, long date, int channelNr, String description, List<T> buffer) {
        super(device, date, channelNr, description, buffer);
        this.scaling = "minmax";
    }

    public String getScaling() {
        return this.scaling;
    }

    public void setScaling(String scaling) {
        if (scaling.equals("linalg") || scaling.equals("logalg")) {
            this.scaling = scaling;
        } else {
            this.scaling = "linalg";
        }
    }
}
