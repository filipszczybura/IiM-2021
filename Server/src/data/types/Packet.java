package data.types;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.Instant;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class Packet implements Serializable {

    protected String device;
    protected String description;
    protected long date;
    protected int channelNr;

    protected Packet() {
        this.device = "Unknown";
        this.description = "Default description";
        this.date = Instant.now().getEpochSecond(); // current UNIX timestamp
        this.channelNr = 0;
    }

    protected Packet(String device, long date, int channelNr, String description) {
        this.device = device;
        this.description = description;
        this.date = date;
        this.channelNr = channelNr;
    }

    public String getDevice() {
        return this.device;
    }

    public String getDescription() {
        return this.description;
    }

    public long getDate() {
        return this.date;
    }

    public int getChannelNr() { return channelNr; }

    public List<Field> getAllClassFields() {
        List<Field> fields = new ArrayList<>();
        for (Class <?> c = this.getClass(); c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    public Field getRequiredField(String parameter) {
        for (Class <?> c = this.getClass(); c != null; c = c.getSuperclass()) {
            for (Field field : c.getDeclaredFields()) {
                if (field.getName().equalsIgnoreCase(parameter)) {
                    return field;
                }
            }
        }
        return null;
    }

    public boolean containsIgnoreCase(Collection<?> keywords) {
        for (Object key : keywords) {
            if (Pattern.compile((String)key, Pattern.CASE_INSENSITIVE + Pattern.LITERAL)
                    .matcher(this.toString())
                    .find()) {
                return true;
            }
        }
        return false;
    }

    public boolean isMatchingEntry(HashMap.Entry<String, Collection<?>> entry) {
        try {
            Field field = this.getRequiredField(entry.getKey());
            field.setAccessible(true);
            if (field.get(this) instanceof String) {
                return this.containsIgnoreCase(entry.getValue());
            } else {
                return entry.getValue().contains(field.get(this));
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Map.Entry<String, Collection<?>>> getFilteredData(HashMap<String, Collection<?>> queries) {
        Set<Map.Entry<String, Collection<?>>> entries = queries.entrySet();
        return entries.stream().filter(this::isMatchingEntry).collect(Collectors.toList());
    }

    public String getEntry() {
        List<String> parameters = new ArrayList<>(Collections.singletonList(this.getClass().getSimpleName()));
        List<String> requiredFields = Arrays.asList("device", "date", "channelNr", "description");
        for (String required : requiredFields) {
            try {
                Field field = this.getRequiredField(required);
                field.setAccessible(true);
                parameters.add(field.get(this).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return String.join(",", parameters);
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null || other.getClass() != this.getClass()) {
            return false;
        }
        Packet otherPacket = (Packet) other;
        return this.toString().equals(otherPacket.toString());
    }

    public String getCommunication(String message) {
        return "%s\n\n%s".formatted(message, this.toString());
    }

    @Override
    public String toString() {
        String className = this.getClass().getSimpleName();
        StringBuilder description = new StringBuilder("Specification of %s:\n".formatted(className));
        List<Field> fields = this.getAllClassFields();
        for (Field field: fields) {
            try {
                field.setAccessible(true);
                String line = String.format(" - %s: %s\n", field.getName(), field.get(this));
                description.append(line);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return description.toString();
    }

}
