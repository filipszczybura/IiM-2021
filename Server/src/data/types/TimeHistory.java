package data.types;

import java.util.List;

public class TimeHistory<T> extends Sequence<T> {

    private double sensitivity;

    public TimeHistory() {
        super();
        this.sensitivity = 0.5;
    }

    public TimeHistory(String device, long date, int channelNr, String description, List<T> buffer) {
        super(device, date, channelNr, description, buffer);
        this.sensitivity = 0.5;
    }

    public TimeHistory(
            String device,
            String description,
            long date,
            int channelNr,
            String unit,
            double resolution,
            List<T> buffer,
            double sensitivity
    ) {
        super(
                device,
                description,
                date,
                channelNr,
                unit,
                resolution,
                buffer
        );
        this.sensitivity = sensitivity;
    }

    public double getSensitivity() {
        return this.sensitivity;
    }

    public void setSensitivity(double sensitivity) {
        this.sensitivity = sensitivity;
    }
}
