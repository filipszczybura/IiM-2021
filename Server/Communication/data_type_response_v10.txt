Server response.
	Device: Huawei MatePad P10s
	Channel number: 14
	Date: 1618349662
	Description: Go with the Mate<Server response>

Specification of Spectrum:
 - scaling: minmax
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [152, 205, 148, 128, 49, 114, 221, 23, 9, 244, 15, 131, 49, 174, 244, 64, 96, 87, 245, 233, 140, 148, 222, 18, 202, 90, 149, 151, 104, 242, 25, 174]
 - device: Huawei P10 Pro
 - description: Astonishing camera lens
 - date: 1618349536
 - channelNr: 6

<Server response>

Specification of Alarm:
 - threshold: 0.5
 - direction: 1
 - device: Huawei P40 Pro
 - description: Be the Pro
 - date: 1618349532
 - channelNr: 9

<Server response>

Specification of TimeHistory:
 - sensitivity: 0.5
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [31, 53, 59, 152, 29, 124, 92, 248, 19, 75, 252, 181, 176, 115, 1, 111, 227, 129, 229, 107, 44, 233, 22, 91, 113, 84, 239, 113, 95, 205, 5, 118]
 - device: Samsung QLED Q800T
 - description: Wow it's so wide
 - date: 1618349309
 - channelNr: 15

<Server response>

Specification of Alarm:
 - threshold: 0.5
 - direction: 1
 - device: Huawei MatePad P10s
 - description: Go with the Mate
 - date: 1618349662
 - channelNr: 14

<Server response>

Specification of Spectrum:
 - scaling: minmax
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [148, 150, 72, 74, 131, 232, 103, 212, 181, 99, 107, 109, 49, 71, 0, 5, 232, 11, 33, 170, 219, 130, 50, 225, 204, 101, 111, 44, 242, 247, 225, 82]
 - device: iPhone XR
 - description: White pearl
 - date: 1618349641
 - channelNr: 21

<Server response>

Specification of TimeHistory:
 - sensitivity: 0.5
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [35, 184, 9, 14, 108, 29, 34, 191, 86, 140, 223, 142, 107, 72, 209, 161, 139, 211, 99, 191, 170, 61, 148, 34, 186, 143, 233, 194, 32, 35, 83, 154]
 - device: Samsung A5 2019
 - description: Durable against extreme conditions
 - date: 1618349423
 - channelNr: 3

<Server response>

Specification of Spectrum:
 - scaling: minmax
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [237, 44, 48, 238, 3, 174, 179, 24, 52, 202, 90, 15, 128, 86, 117, 37, 213, 214, 127, 72, 5, 204, 239, 230, 58, 85, 173, 11, 178, 40, 88, 18]
 - device: MacBook Air 2020
 - description: Light as air
 - date: 1618349392
 - channelNr: 2

<Server response>

Specification of Spectrum:
 - scaling: minmax
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [143, 159, 19, 61, 123, 88, 91, 184, 117, 201, 224, 52, 55, 185, 225, 82, 174, 217, 126, 54, 240, 73, 17, 123, 79, 115, 77, 237, 210, 161, 248, 132]
 - device: Samsung S21
 - description: New arrival
 - date: 1618348421
 - channelNr: 1

<Server response>

Specification of Alarm:
 - threshold: 0.5
 - direction: 1
 - device: Xiaomi Mi9
 - description: Best quality in its price range!
 - date: 1618349556
 - channelNr: 8

<Server response>

Specification of TimeHistory:
 - sensitivity: 0.5
 - unit: Queried unit
 - resolution: 0.05
 - buffer: [35, 112, 212, 36, 53, 106, 256, 52, 102, 140, 152, 102, 169, 238, 138, 44, 96, 22, 19, 167, 37, 17, 78, 157, 18, 206, 34, 19, 118, 24, 39, 240]
 - device: Samsung Galaxy S7
 - description: Black pearl
 - date: 1618349506
 - channelNr: 12

