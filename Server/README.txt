1. DataServer should be run from the com.main.DataServer class, hence the UDPServer and UDP Client work on separate
    threads.

2. There is one mandatory configuration parameter, which is the preferred IPv4 address of your local machine:
   >> cmd:\ -> ipconfig/all -> IPv4 (Preferred)

3. After running the com.main.DataServer.main() method, user is supposed to give the path to comma separated text file
    containing the database:
      - Text file must include data.types parameters. Template line of database should consist of comma separated
      parameters:
        "DataType (Class),Device,Date (Unix timestamp),ChannelNr,Description"
      - Input can be omitted; in such a case, the default Server/Communication/database.txt file will be used.

4. At the beginning there is option to reload previous session from the backup:
    - If yes, user should provide:
        - either date to a specific backup session (%Y%m%d%H%M%S)
        - path to the backup file
        - or press enter in order to load last backup session
    - Otherwise, suitable matches will be found based on queries

5. Then user is asked whether to search for any specific data in the server database:
    - If yes:
        - Required queries should be provided, in the form of comma (exceptionally with additional space) separated
        keywords:
            - Device: .., ...
            - ChannelNr: .., ...
            - Date (unix timestamp): .., ...
            - Description: .., ...
        - Information whether to search for entries based on union or intersection between database and queries:
            - Union [n]: entry matches any query
            - Intersection (y): entry matches all queries
    - Otherwise, a complete database will be loaded.

6. Last step before creating a backup is to update the queries:
    - If user answers yes:
        - User can choose the update method:
            - [y]es, append to the current database
            - (n)o, create a new database and a new absolute filename is required
        - A new entry must consist of 5 comma (additionally with space) separated parameters:
            "DateType (Class), Device, Date (Unix timestamp), ChannelNr, Description"
    - Otherwise, database remains unchanged

7. Before termination of the com.main.DataServer.main(), user has an option to create a backup:
    - If yes, a backup is automatically created
    - Otherwise, the user is notified about disconnection from the server.