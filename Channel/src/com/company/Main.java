package com.company;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Channel channel = new Channel(
                "MyChannel",
                32,
                0,
                250,
                0.5,
                1e-1,
                48000,
                1000
        );
        System.out.printf("Welcome to %s!\n\n", channel.name);
        System.out.printf("Rescaled value (rescaling factor is 2.5): %.2f\n", channel.value(0, 2.5));
        System.out.printf("Average: %.2f\n", channel.mean());
        System.out.printf("RMS: %.2f\n", channel.RMS());
        System.out.print(channel.toString());
    }

    public static int generateRandomInteger(int min, int max) {

        return new Random().nextInt((max + 1) - min) + min;
    }

    public static class Channel {

        static int numberOfChannels = 0;

        int id; // channel number
        String name; // text description
        int bits; // amount of bits of AC converter
        int rangeMin, rangeMax; // min and max value of input voltage range
        double sensitivity; // sensitivity is the ratio of registered amplitude to voltage
        double unit; // unit of registered value
        int samplingFrequency; // sampling frequency
        int numberOfSamples; // number of samples
        List<Short> samples; // array containing values of samples

        public Channel() {
            this.id = ++Channel.numberOfChannels;
            this.name = "Default name";
            this.bits = 32;
            this.rangeMin = 0;
            this.rangeMax = 1;
            this.sensitivity = 0.5;
            this.unit = 1e-1;
            this.samplingFrequency = 48000;
            this.numberOfSamples = 1000;
            this.samples = Arrays.stream(new Short[numberOfSamples])
                    .map(el -> (short) generateRandomInteger(this.rangeMin, this.rangeMax))
                    .collect(Collectors.toList());
        }

        public Channel(
                String name,
                int bits,
                int rangeMin,
                int rangeMax,
                double sensitivity,
                double unit,
                int samplingFrequency,
                int numberOfSamples
        ) {
            this.id = ++Channel.numberOfChannels;
            this.name = name;
            this.bits = bits == 32 || bits == 64 ? bits : 32;
            this.rangeMin = Math.max(rangeMin, 0);
            this.rangeMax = Math.max(rangeMax, rangeMin);
            this.sensitivity = sensitivity;
            this.unit = unit;
            this.samplingFrequency = samplingFrequency;
            this.numberOfSamples = numberOfSamples;
            this.samples = Arrays.stream(new Short[numberOfSamples])
                    .map(el -> (short) generateRandomInteger(this.rangeMin, this.rangeMax))
                    .collect(Collectors.toList());
        }

        public double mean() {
            double sum = this.samples.stream().mapToDouble(Short::doubleValue).sum();
            return sum / this.samples.size();
        }

        public double value(int idx, double rescalingParameter) {
            return this.samples.get(idx) * rescalingParameter;
        }

        public double RMS() {
            double sumOfSquaredErrors = this.samples.stream()
                    .map(el -> Math.pow(el, 2))
                    .mapToDouble(Double::doubleValue)
                    .sum();
            return Math.sqrt(sumOfSquaredErrors / this.samples.size());
        }

        @Override
        public String toString() {
            StringBuilder description = new StringBuilder("\nChannel Description:\n");
            for (Field field : this.getClass().getDeclaredFields()) {
                try {
                    String line = String.format("\t%s: %s\n", field.getName(), field.get(this));
                    description.append(line);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return description.toString();
        }
    }
}
