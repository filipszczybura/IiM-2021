package com.mmorpg

abstract class Mob(
                    var name: String,
                    var healthPoints: Int,
                    var attackPower: Int,
                    var defense: Int
                  ) {

  var level: Int = 1
  var isAlive: Boolean = true

  val attackBooster: Map[String, Int]

  def boostAttack(attributes: List[Int]): List[Int] = {
    attributes.map((x: Int) => x + this.attackPower)
  }

  def attack(opponent: Mob): Unit
  def getSound: String
}