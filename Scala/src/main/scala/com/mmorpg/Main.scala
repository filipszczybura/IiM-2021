package com.mmorpg

object Main extends App {
  val paladin: Human = new Human(
    "Grzegorz",
    150,
    200,
    56, 90,
    20, 15,
    12,
    "Paladin",
    "Jinno"
  )
  println(paladin.toString)
}
