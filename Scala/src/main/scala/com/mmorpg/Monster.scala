package com.mmorpg

class Monster(
               _name: String,
               _healthPoints: Int,
               _attackPower: Int,
               _defense: Int,
               toxicity: Int,
               brutality: Int,
               confusion: Int,
               curse: Int,
               var kind: String
             ) extends Mob(
  _name: String,
  _healthPoints: Int,
  _attackPower: Int,
  _defense: Int
) {

  override val attackBooster: Map[String, Int] = (
    List(
      "Black Widow",
      "Grizzly Bear",
      "Abandoned Soul",
      "Rotten Undead"
    ) zip boostAttack(List(
      this.toxicity,
      this.brutality,
      this.confusion,
      this.curse
    ))
    ).toMap

  def apply(
             name: String,
             healthPoints: Int,
             attackPower: Int,
             defense: Int,
             toxicity: Int,
             brutality: Int,
             confusion: Int,
             curse: Int,
             kind: String = ""
           ) {
    this.apply(name, healthPoints, attackPower, defense, toxicity, brutality, confusion, curse, kind)
    this.kind = chooseKind(kind)
  }

  def chooseKind(kind: String): String = {
    for (availableKind <- attackBooster.keySet) {
      if (availableKind.equalsIgnoreCase(kind)) {
        return availableKind
      }
    }
    "Wild animal"
  }

  override def attack(opponent: Mob): Unit = {
    val damage: Int = this.attackBooster.getOrElse(this.kind, 0) - opponent.defense
    if (damage >= opponent.healthPoints) {
      opponent.healthPoints = 0
      opponent.isAlive = false
    } else {
      opponent.healthPoints -= damage
    }
  }

  override def getSound: String = {
    this.kind match {
      case "Black Widow" => "One can hear some web winding in the woods..."
      case "Grizzly Bear" => "Growling at the river never ends well..."
      case "Abandoned Soul" => "Blessed are those who haven't seen, but believed..."
      case "Rotten Undead" => "Your stomach is rocked by the unmerciful stench..."
      case _ => "The local population strongly advises against approaching this wild animal..."
    }
  }
}