package com.mmorpg

import scala.util.Random

class Human(
             _name: String,
             _healthPoints: Int,
             _attackPower: Int,
             _defense: Int,
             var strength: Int,
             var endurance: Int,
             var agility: Int,
             var intelligence: Int,
             var profession: String,
             var kingdom: String
           ) extends Mob(
  _name: String,
  _healthPoints: Int,
  _attackPower: Int,
  _defense: Int
) {

  var experience: Double = .0
  var gold: Double = .0
  var toNextLevel: Double = 100 * this.level

  var friends: Set[Human] = Set()
  var quests: Map[String, Map[String, Double]] = Map()

  override val attackBooster: Map[String, Int] = (
    List(
      "Knight",
      "Paladin",
      "Mage",
      "Rogue"
    ) zip boostAttack(List(
      this.strength,
      this.endurance,
      this.intelligence,
      this.agility))
    ).toMap

  def apply(
             name: String,
             healthPoints: Int,
             attackPower: Int,
             defense: Int,
             strength: Int,
             endurance: Int,
             agility: Int,
             intelligence: Int,
             profession: String = "",
             kingdom: String = ""
           ) {
    this.apply(name, healthPoints, attackPower, defense, strength, endurance, agility, intelligence, profession, kingdom)
    this.profession = chooseProfession(profession)
    this.kingdom = chooseKingdom(kingdom)
  }

  def takeOnQuest(questName: String, description: String, reward: Double): Unit = {
    if (!this.quests.keySet.contains(questName)) {
      val newQuest: Map[String, Double] = Map(description -> reward)
      this.quests += questName -> newQuest
    }
  }

  def closeQuest(questName: String, isFinished: Boolean): Unit = {
    if (this.quests.keySet.contains(questName)) {
      if (isFinished) {
        val reward: Double = this.quests.getOrElse(questName, Map()).values.toList.head
        this.gold += reward
        print(s"You have successfully finished the '$questName' and received $reward gold!")
      } else {
        print(s"You gave up on '$questName'")
      }
    } else {
      println(s"There is no active '$questName' quest!")
    }
  }

  def filterFriends(name: String = "", level: Int = 0): Set[Human] = {
    var filtered: Set[Human] = this.friends
    if (name.nonEmpty) {
      filtered = filtered.filter(friend => friend.name.equalsIgnoreCase(name))
    }
    if (level > 0) {
      filtered = filtered.filter(friend => friend.level == level)
    }
    filtered
  }

  def chooseProfession(profession: String): String = {
    for (availableProfession <- this.attackBooster.keySet) {
      if (profession.equalsIgnoreCase(availableProfession)) {
        return availableProfession
      }
    }
    "Adventurer"
  }

  def chooseKingdom(kingdom: String): String = {
    val kingdoms: Set[String] = Set("Shinsoo", "Jinno", "Chunjo")
    if (kingdoms.contains(kingdom)) kingdom else kingdoms.toList(Random.nextInt(kingdoms.size))
  }

  override def attack(opponent: Mob): Unit = {
    val damage: Int = this.attackBooster.getOrElse(this.profession, 0) - opponent.defense - opponent.defense
    if (damage >= opponent.healthPoints) {
      opponent.healthPoints = 0
      opponent.isAlive = false
      val experienceGained = opponent.level * 25
      this.experience += experienceGained
      this.gainExperience(experienceGained)
    } else {
      opponent.healthPoints -= damage
    }
  }

  def addToFriends(human: Human): Unit = {
    this.friends += human
    println(s"Greetings to ${human.name}! Let God bless your adventure!")
  }

  def gainExperience(experience: Double): Unit = {
    var remainingExperience: Double = experience
    while (remainingExperience > 0) {
      if (remainingExperience >= this.toNextLevel) {
        this.level += 1
        this.attackPower += 5
        this.defense += 10
        this.healthPoints += 25
        this.toNextLevel = this.level * 100
        remainingExperience -= this.toNextLevel
        this.profession match {
          case "Knight" => this.strength += 2
          case "Paladin" => this.endurance += 2
          case "Mage" => this.intelligence += 2
          case "Rogue" => this.agility += 2
        }
      } else {
        this.toNextLevel -= remainingExperience
        remainingExperience = 0
      }
    }
  }

  override def getSound: String = {
    s"I fight with honor to protect my Kingdom. Hail to the sacred $kingdom!"
  }

  override def toString: String = {
    s"${this.name} is ${this.level} level Human ${this.profession} from ${this.kingdom}"
  }

}
