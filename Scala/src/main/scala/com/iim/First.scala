package com.iim
import scala.math.{log, pow, cbrt}

object First extends App {

  def starRectangle(x: Int, y: Int): String = {
    val arr = for (_ <- 1 to y) yield "*" * x
    arr.mkString("\n")
  }

  def magicCubes(): IndexedSeq[Int] = {
    for (i <- 100 to 999 if i.toString.map(_.asDigit).map(pow(_, 3)).sum == i)
      yield i
  }

  def factorial(n: BigInt): BigInt = {
    if (n <= 1) {
      1
    } else {
      n * factorial(n - 1)
    }
  }

  val starRectangle: String = starRectangle(8, 3)
  val w: Double = cbrt(2 * log(521) + (7.5e8 / (128 * 422)))

  println(f"Star Rectangle:\n$starRectangle")
  println(f"Result of the equation sqrt(2log(521) + 7.5e8 / (128 * 422)) is $w%.2f")
  println(
    f"3-digit numbers, in which sum of elementwise cubes is equal to that number: " +
    f"[" + magicCubes().mkString(", ") + "]"
  )
  val factorials = for(i <- 1 to 10) yield println(s"Factorial of $i is ${factorial(i)}")
}