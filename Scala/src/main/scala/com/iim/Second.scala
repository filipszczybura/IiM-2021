package com.iim

import java.util.regex.Pattern

object Second extends App {

  class Person(var name: String, var surname: String, var age: Int, var isVaccinatedSarsCov19: Boolean) {
    var SarsCov19Vaccinations: Int = if (this.isVaccinatedSarsCov19) 2 else 0
    def this(name: String, surname: String, age: Int) {
      this(name, surname, age, false)
      this.name = name
      this.surname = surname
      this.age = age
    }

    def vaccineAgainstSarsCov19(): Unit = {
      if (this.SarsCov19Vaccinations < 2) {
        this.SarsCov19Vaccinations += 1
        if (this.SarsCov19Vaccinations == 2) {
          this.isVaccinatedSarsCov19 = true
        }
      } else {
        println(s"$name $surname cannot exceed two SarsCov19 vaccine doses!")
      }
    }

    override def toString: String = s"" +
      s"Name: $name, " +
      s"Surname: $surname, " +
      s"SarsCov19 Vaccines: $SarsCov19Vaccinations/2"
  }

  class Car(var brand: String, var model: String, var mileage: Int, var price: Int) {
    var location: Array[Int] = Array(0, 0)
    var velocity: Double = .0
    var isOn: Boolean = false

    def this(model: String, mileage: Int, price: Int) {
      this("Description", model, mileage, price)
      this.model = model
      this.mileage = mileage
      this.price = price
    }

    def changeStatus(status: String): Unit = {
      if (Pattern.compile("start", Pattern.CASE_INSENSITIVE).matcher(status).matches()) {
        this.isOn = true
        this.velocity = 0
      } else if (Pattern.compile("stop", Pattern.CASE_INSENSITIVE).matcher(status).matches()) {
        this.isOn = false
        this.velocity = 0
      }
    }

    def drive(direction: Char): Unit = {
      if (direction == 'L') {
        this.location(0) -= 1
      } else if (direction == 'R') {
        this.location(0) += 1
      }
      if (direction == 'U') {
        this.location(1) += 1
      } else if (direction == 'D') {
        this.location(1) -= 1
      }
    }

    def accelerate(rate: Double, seconds: Double): Unit = {
      if (!this.isOn) {
        this.changeStatus("start")
      }
      if (rate > 0) {
        this.velocity += rate * seconds
      } else {
        if (this.velocity - (rate * seconds) > 0) {
          this.velocity -= rate * seconds
        } else {
          this.velocity = 0
        }
      }
    }

    def mileageGuard(minMileage: Int, maxMileage: Int): String = {
      mileage match {
        case idealMileage: Int if this.mileage <= maxMileage & this.mileage >= minMileage => s"$idealMileage km is a perfect mileage!"
        case underMileage: Int if this.mileage < minMileage => s"$underMileage km mileage is too mow!"
        case overMileage: Int if this.mileage > maxMileage => s"$overMileage km mileage is too high!"
        case _ => "Unrecognized mileage!"
      }
    }

    def dropPrice(percentage: Float): Unit = { this.price *= percentage.round }
    override def toString: String = "%s %s: %d$".format(brand, model, price)
  }

  def whatObjectIsIt(instance: Any): String = {
    val msg = "It's a %s object!"
    instance match {
      case car: Car => msg.format(car.getClass.getName)
      case person: Person => msg.format(person.getClass.getName)
      case _ => msg.format("user-undefined")
    }
  }

  def isItNumeric(variable: Any): Boolean = {
    variable match {
      case _: Int => true
      case _: Float => true
      case _: Double => true
      case _: Long => true
      case _: Short => true
      case _ => false
    }
  }

  val volvo = new Car("Volvo", "V70R", 145600, 35000)
  println(volvo)
  volvo.accelerate(0.5, 20)
  println(s"Car's velocity: ${volvo.velocity} m/s")
  println(volvo.mileageGuard(120000, 150000))
  volvo.changeStatus("stop")
  print("\n")

  val adam = new Person("Adam", "Kowalski", 27)
  println(Array(
    adam.name,
    "is",
    if(adam.isVaccinatedSarsCov19) "" else "not",
    "vaccinated against SarsCov19"
  ).mkString(" "))
  for (_ <- 1 to 2) {
    adam.vaccineAgainstSarsCov19()
  }
  println(adam)

  val x: Int = 50
  println(whatObjectIsIt(x))
  val numericComment = "%s of a numeric data type"
  println(if (isItNumeric(x)) numericComment.format(s"$x is") else numericComment.format(s"$x is not"))
}
