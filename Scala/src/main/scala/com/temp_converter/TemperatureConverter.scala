package com.temp_converter

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.paint.Color._
import scalafx.scene.shape.Rectangle
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.event.ActionEvent
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.TextFormatter.Change
import scalafx.scene.control.{Label, TextArea, TextField, TextFormatter}
import scalafx.scene.text.Text
import scalafx.scene.layout._
import scalafx.util.StringConverter
import scalafx.scene.control.Button
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._

object TemperatureConverter extends JFXApp {

  def toInt(s: String): Int = {
    try {
      s.toInt
    } catch {
      case e: Exception => 0
    }
  }

  def isNumeric(str:String): Boolean = str.matches("[-+]?\\d+(\\.\\d+)?")

  def CheckInput(input:String):Double={
    if(isNumeric(input)){
      input.toDouble
    }
    else{
      new Alert(AlertType.Information, "Wrong input!").showAndWait()
      0
    }
  }


  val FTextField = new TextField

  val CTextField = new TextField

  val c2fBut: Any = new Button{
    text = "C to F"
    onAction = (a: ActionEvent) => {
      val goodIn = "[0-9]+".r
      val inpc = CTextField.text()
      val c=CheckInput(inpc)
      val cel = c*1.8 + 32
      FTextField.text = cel.toString
    }
  }


  val f2cButton: Any = new Button{
    text = "F to C"
    onAction = (a: ActionEvent) => {
      val goodIn = "[0-9]+".r
      val inp = FTextField.text()
      val f=CheckInput(inp)
      val far = (f- 32)/1.8
      CTextField.text = far.toString
    }
  }

  stage = new JFXApp.PrimaryStage {
    scene = new Scene(300,200) {
      fill = LightGreen

      root = new VBox {
        children = Seq(
          new Text {
            text = "Temperature in C deg:" +CTextField.text()
            style = "-fx-font-size: 8pt"
          },
          new BorderPane {
            top = CTextField
          },
          new Text {
            text = "Temperature in F: "
            style = "-fx-font-size: 8pt"
          },
          new BorderPane {
            top = FTextField
          },
          new BorderPane {
            center = c2fBut
            style="-fx-padding: 20px"
          },
          new BorderPane {
            center = f2cButton
          }
        )

      }
    }
  }
}