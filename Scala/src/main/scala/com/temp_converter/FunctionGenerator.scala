package com.temp_converter

import scala.collection.StringOps


object FunctionGenerator extends JFXApp {

  // Defining the axes
  val xAxis = new NumberAxis()
  xAxis.label = "X axis"
  val yAxis = new NumberAxis()
  yAxis.label = "Y axis"
  val aText: Any = new TextField{
    text = "1"
  }
  val bText: TextField = new TextField{
    text = "1"
  }
  val cText: TextField = new TextField{
    text = "1"
  }
  val dText: TextField = new TextField{
    text = "1"
  }
  val range1: TextField = new TextField{
    text = "0"
  }
  val range2: TextField = new TextField{
    text = "50"
  }

  var dataInc =1

  def isAllDigits(x: String): Boolean = x forall Character.isDigit

  def ParseInput(input: String): Double = {
    val numPattern = "[0-9]+".r
    val charPattern = "[^0-9.-]+".r
    val parsed_input = charPattern.findAllIn(input).mkString
    var a:Double = 0
    if(isAllDigits(parsed_input)){
      if(input contains "-"){
        if(input contains "."){
          val aindot = input.split("\\.")
          print(aindot(0)+ " : " + aindot(1)+"\n")
          val anum = numPattern.findAllIn(aindot(0)).mkString
          a = -1 * (anum.toDouble+aindot(1).toDouble / 10)
        }
        else{
          val anum = numPattern.findAllIn(input).mkString
          a = -1*anum.toDouble
        }
      }
      else{
        if(input contains "."){
          val aindot = input.split("\\.")
          print(aindot(0)+ " : " + aindot(1)+"\n")
          val anum = numPattern.findAllIn(aindot(0)).mkString
          a = anum.toDouble+aindot(1).toDouble/10
        }
        else{
          val anum = numPattern.findAllIn(input).mkString
          a = anum.toDouble
        }
      }
      a
    }
    else{
      new Alert(AlertType.Information, "put numbers only").showAndWait()
      return 0
    }
  }

  val linPlot: Button = new Button{
    text = "ax+b"
    onAction = (a: ActionEvent) => {
      val a = ParseInput(aText.text())
      val b = ParseInput(bText.text())
      val x1 = ParseInput(range1.text())
      val x2 = ParseInput(range2.text())

      var plot_data: Seq[(Double,Double)] = Seq((x1,a*x1+b))
      for(i <- x1.toInt to x2.toInt){
        plot_data = plot_data :+ (i*1.0,a*i.toDouble+b)
      }

      val data = ObservableBuffer(plot_data) map {case (x, y) => XYChart.Data[Number, Number](x, y)}
      val series = XYChart.Series[Number, Number]("data"+dataInc.toString(), data)
      lineChart.getData.add(series)
      dataInc = dataInc +1
    }
  }

  val powPlot: Button = new Button{
    text = "ax3+bx2+cx+d"
    onAction = (a: ActionEvent) => {

      val a = ParseInput(aText.text())
      val b = ParseInput(bText.text())
      val c = ParseInput(cText.text())
      val d = ParseInput(dText.text())
      val x1 = ParseInput(range1.text())
      val x2 = ParseInput(range2.text())

      var plot_data: Seq[(Double,Double)] = Seq((x1,a*x1*x1*x1+b*x1*x1+c*x1+d))
      for(i <- x1.toInt to x2.toInt){
        plot_data = plot_data :+ (i*1.0,a*i*i*i+b*i*i+c*i+d)
      }

      val data = ObservableBuffer(plot_data) map {case (x, y) => XYChart.Data[Number, Number](x, y)}
      val series = XYChart.Series[Number, Number]("data" + dataInc.toString(), data)
      lineChart.getData.add(series)
      dataInc = dataInc +1
    }
  }


  val clearButton: Button = new Button{
    text = "clear"
    onAction = (a: ActionEvent) => {
      lineChart.getData.clear()
    }
  }

  // Creating the chart
  val lineChart: LineChart = LineChart(xAxis, yAxis)
  val borderStyle: String = "" +
    "-fx-background-color: blue;" +
    "-fx-padding: 5;"+
    "-fx-margin: 40;"

  val secondStyle: String = "" +
    "-fx-background-color: yellow;" +
    "-fx-padding: 5;"+
    "-fx-margin: 40;"
  lineChart.title = "Plot function:"




  stage = new JFXApp.PrimaryStage {
    title.value = "Function"
    scene = new Scene(900,600) {

      root = new BorderPane {
        center = lineChart
        right = new HBox {
          style="-fx-padding: 20;"
        }
        right = new VBox {
          children = Seq(
            new HBox{
              style=borderStyle
              children = Seq(
                new Text {
                  text = "a: "
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = aText
                }
              )
            },
            new HBox{
              style=borderStyle
              children = Seq(
                new Text {
                  text = "b: "
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = bText
                }

              )

            },
            new HBox{
              style=borderStyle
              children = Seq(
                new Text {
                  text = "c: "
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = cText
                }
              )
            },
            new HBox{
              style=borderStyle
              children = Seq(
                new Text {
                  text = "d: "
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = dText
                }
              )
            },
            new HBox{
              style=borderStyle
              children = Seq(
                new Text {
                  text = "plot:"
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = linPlot
                },
                new BorderPane {
                  top = powPlot
                },
                new BorderPane {
                  top = clearButton
                }
              )
            },
            new HBox{
              style=secondStyle
              children = Seq(
                new Text {
                  text = "range from:"
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = range1
                  //style=secondStyle
                }
              )
            },
            new HBox{
              style=secondStyle
              children = Seq(
                new Text {
                  text = "to:"
                  style = "-fx-font-size: 14pt"
                },
                new BorderPane {
                  top = range2

                }
              )
            }
          )
        }
      }
    }
  }
}