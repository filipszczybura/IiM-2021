package temperature_converter

// Temperature converter

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.event.ActionEvent
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Button, TextField, _}
import scalafx.scene.layout._
import scalafx.scene.paint.Color._
import scalafx.scene.text.Text

object TemperatureConverter extends JFXApp {

  def isNumeric(str: String): Boolean = str.matches("[-+]?\\d+(\\.\\d+)?")

  def CheckInput(input: String):Double={
    if(isNumeric(input)){
      input.toDouble
    }
    else{
      new Alert(AlertType.Information, "Wrong input!").showAndWait()
      0
    }
  }


  val FTextField = new TextField

  val CTextField = new TextField

  val c2fBut: Button = new Button{
    text = "Celcius to Fahrenheit"
    onAction = (_: ActionEvent) => {
      val input = CheckInput(CTextField.text())
      val celcius = input * 1.8 + 32
      FTextField.text = celcius.toString
    }
  }


  val f2cButton: Button = new Button{
    text = "Fahrenheit to Celcius"
    onAction = (_: ActionEvent) => {
      val input = CheckInput(FTextField.text())
      val fahrenheit = (input - 32) / 1.8
      CTextField.text = fahrenheit.toString
    }
  }

  stage = new JFXApp.PrimaryStage {
    scene = new Scene(300,200) {
      fill = LightGreen

      root = new VBox {
        children = Seq(
          new Text {
            text = "Temperature in °C" +CTextField.text()
            style = "-fx-font-size: 8pt"
          },
          new BorderPane {
            top = CTextField
          },
          new Text {
            text = "Temperature in °F"
            style = "-fx-font-size: 8pt"
          },
          new BorderPane {
            top = FTextField
          },
          new BorderPane {
            center = c2fBut
            style="-fx-padding: 20px"
          },
          new BorderPane {
            center = f2cButton
          }
        )
      }
    }
  }
}
